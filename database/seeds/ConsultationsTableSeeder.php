<?php

use Illuminate\Database\Seeder;

class ConsultationsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

// 2019-01-01-től minden páros héten hétfő 8-12 óra

        DB::table('consultations')->insert([
            'from' => \Carbon\Carbon::parse('2019-01-01'),
            'week_type' => 'even',
            'day' => 1,
            'time' => '8-12 óra',
        ]);

// 2019-01-01-től minden páratlan héten szerda 12-16 óra
        DB::table('consultations')->insert([
            'from' => \Carbon\Carbon::parse('2019-01-01'),
            'week_type' => 'odd',
            'day' => 3,
            'time' => '12-16 óra',
        ]);

// 2019-01-01-től minden héten pénteken 10-14 óra
        DB::table('consultations')->insert([
            'from' => \Carbon\Carbon::parse('2019-01-01'),
            'week_type' => 'all',
            'day' => 5,
            'time' => '10-14 óra',
        ]);

// 2019-06-01-től 2019-07-31-ig minden héten kedden 16-20 óra
        DB::table('consultations')->insert([
            'from' => \Carbon\Carbon::parse('2019-06-01'),
            'to' => \Carbon\Carbon::parse('2019-07-31'),
            'week_type' => 'all',
            'day' => 2,
            'time' => '16-20 óra',
        ]);
    }
}
