<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="_token" content="{{csrf_token()}}" />

        <title>Teszt Ónody</title>

        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/css/bootstrap.min.css">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.0/jquery.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/js/bootstrap.min.js"></script>

        <script type="text/javascript" src="https://cdn.jsdelivr.net/momentjs/latest/moment.min.js"></script>
        <script type="text/javascript" src="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.min.js"></script>
        <link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.css" />
    </head>
    <body>

    <div class="row">
        <div class="col-md-4 col-md-offset-4">

            <div class="panel panel-primary">
                <div class="panel-heading">Válaszd ki a dátumokat</div>
                <div class="panel-body">
                    <input type="text" name="daterange" value="2019-05-01 - 2019-05-29" />
                    <button type="button" class="btn btn-info btn-lg" data-toggle="modal" data-target="#myModal" id="send">Küldés</button>
                </div>
            </div>

            <div class="modal fade" id="myModal" role="dialog">
                <div class="modal-dialog">

                    <!-- Modal content-->
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                            <h4 class="modal-title">Munkanapok</h4>
                        </div>
                        <div class="modal-body">

                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-default" data-dismiss="modal">Vissza</button>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>


    </body>

    <script>
        $( document ).ready(function() {

            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
                }
            });

            var dateRange = $('input[name="daterange"]').daterangepicker({
                opens: 'left',
                locale: {
                    format: 'YYYY-MM-DD'
                }
            });

            $('#send').click(function(){

                $.ajax({
                    type:'POST',
                    url:'getdays',
                    data:{'dateRange': dateRange.val()},
                    success:function(res) {
                        $(".modal-body").html(res.msg);
                    }
                });
            });
        });
    </script>

</html>
