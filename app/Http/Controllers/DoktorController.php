<?php

namespace App\Http\Controllers;

use App\Consultation;
use Carbon\CarbonPeriod;
use Illuminate\Http\Request;

class DoktorController extends Controller
{

    public function index()
    {
        return view('index');
    }

    public function getDays(Request $request){

        $dateRange = $request->dateRange;

        $dates = explode(" - ",$dateRange);

        $weekMap = [
            0 => 'Vasárnap',
            1 => 'Hétfő',
            2 => 'Kedd',
            3 => 'Szerda',
            4 => 'Csütörtök',
            5 => 'Péntek',
            6 => 'Szombat',
        ];

        $period = CarbonPeriod::create($dates[0] , $dates[1]);


// 1. verzio

/*
        $msg = '';
        foreach ($period as $date) {

            $dayOfWeek = $date->dayOfWeek;

            $weekOfYear = $date->weekOfYear;

            if($weekOfYear %2){
                $weekType = 'odd';
            }else{
                $weekType = 'even';
            }

            $consultations = Consultation::where('from', '<=', $date.' 00:00:00')->where('to', '>=', $date.' 00:00:00')->where('day', $dayOfWeek)->get();
            $times = '';

            foreach($consultations as $consultation){
                if($consultation->week_type == 'all' || $consultation->week_type == $weekType){
                    $times .= $consultation->time .', ';
                }
            }

            if($times != ''){
                $msg .= $date->format('Y-m-d') . " - " . $weekMap[$dayOfWeek] . ' : ' . $times;
                $msg .= "<br>";
            }
        }
        return response()->json([
            'success' => true,
            'msg' => $msg
        ]);
*/

// 2. verzio
        $msg = [];

        $consultations = Consultation::all();

        foreach($consultations as $consultation){

            foreach ($period as $date) {

                $dayOfWeek = $date->dayOfWeek;

                $weekOfYear = $date->weekOfYear;

                if($weekOfYear %2){
                    $weekType = 'odd';
                }else{
                    $weekType = 'even';
                }

                $times = '';

                if( $consultation->day == $dayOfWeek && $consultation->from <= $date.' 00:00:00' && $consultation->to >= $date.' 00:00:00' && ($consultation->week_type == 'all' || $consultation->week_type == $weekType) ){
                    $times .= $consultation->time .', ';
                }

                if($times != ''){
                    array_push($msg, $date->format('Y-m-d') . " - " . $weekMap[$dayOfWeek] . ' : ' . $times . "<br>");
                }
            }
        }

        sort($msg);

        return response()->json([
            'success' => true,
            'msg' => implode(" ",$msg)
        ]);
    }

}
